import csv

#inicials
hipoteca = 10000
mensualidad = 375
pago_anual = 500
interes = 0.005

#variables
mes = 1
dinero = hipoteca
interes_pagado = 0

#Crear csv
with open('Documento.csv', mode='w', newline='') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(['Month','Interest','Due','Due+','Paid'])

    while dinero > 0:
        pago_mensual = dinero * interes
        interes_pagado += pago_mensual

        if mes % 12 == 0:
            pago_total = mensualidad + pago_anual
        else:
            pago_total = mensualidad
        
        #detalles en csv
        writer.writerow([mes, round(pago_mensual,2), round(dinero,2), round(dinero + pago_mensual,2), round(pago_total,2)])

        #nuevo dinero
        dinero += pago_mensual - pago_total

        mes += 1